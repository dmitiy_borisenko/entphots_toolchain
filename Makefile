INC := inc
SRC := src
BLD := bld

# DICTs
TOOLCHAIN_DICT := $(BLD)/EntphotsToolchain.cxx

# LIBs
TOOLCHAIN_LIB := $(BLD)/libEntphotsToolchain.so

CLING = rootcling -I$(realpath $(INC)) -f $@ -c $(foreach hdr,$^,$(notdir $(hdr)))
COMPILER := g++ $(shell root-config --cflags --libs) -I./inc 
LIB_COMPILER = $(COMPILER) -shared -fPIC -o $@ $^

$(TOOLCHAIN_LIB): $(SRC)/WfReader.cc $(TOOLCHAIN_DICT)
	$(LIB_COMPILER)

$(TOOLCHAIN_DICT):: $(INC)/WfReader.h LinkDef.h
	$(CLING)

.PHONY: clean
clean:
	@find $(BLD)/* -exec rm -vf {} \;