#include <Rtypes.h>

class TFile;
class TTree;

class DataProcessor {
protected:
    TFile *f;
    TTree *t;
    Long64_t ev;
    DataProcessor *ancestor;

    DataProcessor();

public:
    virtual bool step() { return false; }
    virtual void process() {}
    DataProcessor &operator|(DataProcessor &follower);
    Long64_t getEv() { return ev; }
};


class WfReader : public DataProcessor {
public:
    WfReader(const char* fname);

    bool step();

ClassDef(WfReader, 0)
};


class Parametrizar : public DataProcessor {
public:
    
    void process(); 

ClassDef(Parametrizar, 0)
};