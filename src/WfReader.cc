#include "WfReader.h"
#include <TFile.h>
#include <TTree.h>

DataProcessor::DataProcessor()
: f()
, t()
, ev(-1)
, ancestor()
{}


DataProcessor &DataProcessor::operator|(DataProcessor &follower)
{
    follower.ancestor = this;
    follower.process();
    return follower;
}


WfReader::WfReader(const char* fname)
{
    f = TFile::Open(fname);
    t = (TTree*) f->Get("adc64_data");
}

bool WfReader::step()
{
    if (!f || ev + 1 >= t->GetEntries()) return false;
    ++ev;
    return true; 
}


void Parametrizar::process()
{
    if (!ancestor) return;

    while (ancestor->step());

    printf("Parametrizar::process(): %lld events processed\n", ancestor->getEv() + 1);
}